import java.lang.System
import java.util.*
import java.util.Scanner

class Pasta(
    var name: String,
    var priceEuros: Double,
    var weightGrams: Double,
    var aproxKcal: Int
)

class Beguda(
    var name: String,
    var priceEuros:Double,
    var sugar: Boolean
){
    init{
        if(sugar) priceEuros*=1.1
    }
}



fun pastisseria(){
    val croissant = Pasta("croissant",1.3, 75.2, 450)
    val ensaimada = Pasta("ensaimada", 1.5, 55.0, 320)
    val donut = Pasta("donut", 1.5, 52.4, 270)
    val pastas3: MutableList<Pasta> = mutableListOf(croissant,ensaimada,donut)
    for(i in pastas3){
        println("Nova pasta creada:\n\n   Nom: ${i.name}\n   Preu(euros): ${i.priceEuros}\n   Pes(grams): ${i.weightGrams}\n   CaloriesAprox(kcal):${i.aproxKcal}\n")
    }
}

fun pastisseria2(){
    val aigua  = Beguda("Aigua", 1.0, false)
    val cafeTallat = Beguda("Café Tallat", 1.35, false)
    val teVermell = Beguda("Té Vermell", 1.5, false)
    val cola = Beguda("CocaCola", 1.65, true)
    val begudes4 : MutableList<Beguda> = mutableListOf(aigua, cafeTallat, teVermell, cola)
    for(i in begudes4){
        println("Nova beguda creada:\n\n   Nom: ${i.name}\n   Preu: ${i.priceEuros}\n")
    }
}





