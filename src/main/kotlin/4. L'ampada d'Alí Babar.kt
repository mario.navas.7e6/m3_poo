const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

class AliBabaLamp(
    var id : String
){
    private var intensity = 1
    private var color = 0
    private val colors = arrayOf(ANSI_WHITE, ANSI_RED, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    private val off = ANSI_BLACK
    var state = true

    fun changeIntensity(){
        if(state){
            if(intensity in 1..4) intensity ++
            println("Lampada: $id - Color: ${colors[color] +" "+ ANSI_RESET} - Intensitat $intensity")
        }
    }
    fun changeColor(){
        if(state){
            if(color==5) color = 0
            else color ++
            println("Lampada: $id - Color: ${colors[color] +" "+ ANSI_RESET} - Intensitat $intensity")
        }
    }

    fun turnOff(){
        state = false
        println("Lampada: $id - Color: ${off +" "+ ANSI_RESET} - Intensitat 0")
    }

    fun turnOn(){
        state = true
        intensity = 1
        println("Lampada: $id - Color: ${colors[color] +" "+ ANSI_RESET} - Intensitat $intensity")
    }
}

fun alibaba(){
    val bathRoomLamp = AliBabaLamp("lavabo")
    val room1Lamp = AliBabaLamp("room1")

    bathRoomLamp.turnOn()
    repeat(3){bathRoomLamp.changeColor()}
    repeat(4){bathRoomLamp.changeIntensity()}

    room1Lamp.turnOn()
    repeat(2){room1Lamp.changeColor()}
    repeat(4){room1Lamp.changeIntensity()    }
    room1Lamp.turnOff()
    room1Lamp.changeColor()
    room1Lamp.turnOn()
    room1Lamp.changeColor()
    repeat(4){room1Lamp.changeIntensity()}

}