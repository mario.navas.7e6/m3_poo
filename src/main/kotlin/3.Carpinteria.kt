import java.util.*

class Taulell(
    var priceSquareMeter: Int,
    var length: Int,
    var width: Int,

    ){
    var price: Int = width*length*priceSquareMeter
}

class Llisto(
    var priceMeter: Int,
    var length: Int,
) {
    var price :Int = length*priceMeter

}

fun carpinteria(){
    val scanner = Scanner(System.`in`)
    println("How many pieces do you want to calculate:")
    val num = scanner.nextLine().toInt()
    var priceT = 0
    var priceL = 0
    repeat(num){
        val piece = scanner.nextLine().split(" ")
        if(piece[0] == "Taulell"){
            val priceSquareMeter = piece[1].toInt()
            val length = piece[2].toInt()
            val width= piece[3].toInt()
            priceT += Taulell(priceSquareMeter, length, width).price
        } else{
            val priceMeter = piece[2].toInt()
            val length = piece[3].toInt()
            priceL += Llisto(priceMeter, length).price
        }
    }
    print("El preu total és ${priceL+priceT}€")
}

