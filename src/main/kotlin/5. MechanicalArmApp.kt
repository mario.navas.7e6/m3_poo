class MechanicalArm(){
    var turnedOn = false
    var openAngle = 0.0
    var altitude = 0.0

    fun changeState(){
        turnedOn = !turnedOn
        println("MechanicalArm{openAngle=${openAngle}, altitude=${altitude}, turnedOn=${turnedOn}}")
    }
    fun updateAngle(num:Int){
        openAngle = num.toDouble()
        println("MechanicalArm{openAngle=${openAngle}, altitude=${altitude}, turnedOn=${turnedOn}}")
    }
    fun updateAltitude(num: Int){
        altitude = num.toDouble()
        println("MechanicalArm{openAngle=${openAngle}, altitude=${altitude}, turnedOn=${turnedOn}}")
    }
}

fun mechanicalArm(){
    var mechanicalArm = MechanicalArm()
    mechanicalArm.changeState()
    mechanicalArm.updateAltitude(3)
    mechanicalArm.updateAngle(180)
    mechanicalArm.updateAltitude(-3)
    mechanicalArm.updateAngle(-180)
    mechanicalArm.updateAltitude(3)
    mechanicalArm.changeState()
}