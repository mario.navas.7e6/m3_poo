open class Electrodomestic(
    var preuBase: Int,
    var consum: String = "A",
    var pes: Int = 5,
    var color: String = "blanc"
    ){

    val colors = arrayOf("blanc", "platejat", "color")
    val consums = arrayOf("A", "B", "C", "D", "E", "F", "G")
    var preuFinal= preuBase

    open fun preuFinal(){
        when(this.consum){
            "A"-> preuFinal +=35
            "B"-> preuFinal +=30
            "C"-> preuFinal +=25
            "D"-> preuFinal +=20
            "E"-> preuFinal +=15
            "F"-> preuFinal +=10
            "G"-> preuFinal +=0
        }

        when(this.pes){
            in 6..20->preuFinal+=20
            in 21..50->preuFinal+=50
            in 51..80->preuFinal+=80
            in 81..100->preuFinal+=100
        }
    }
}

class Rentadora( preuBase: Int,
                 consum: String = "A",
                 pes: Int = 5,
                 color: String = "blanc",
                 var carrega: Int = 5): Electrodomestic(preuBase){
    override fun preuFinal(){
        when(this.carrega){
            in 6..7-> preuFinal += 55
            8-> preuFinal += 70
            9-> preuFinal += 85
            10-> preuFinal += 100
        }
    }
}

class Televisio(preuBase: Int,
                consum: String = "A",
                pes: Int = 5,
                color: String = "blanc",
                var tamany: Int = 28): Electrodomestic(preuBase){
    override fun preuFinal(){
        when(this.tamany){
            in 29..32-> preuFinal += 50
            in 33..42-> preuFinal +=100
            in 43..50-> preuFinal +=150
            in 50..100->preuFinal +=200
        }
    }
}

fun electrodomestics() {
    var electrodomestic1 = Electrodomestic(35, "A", 2, "blanc")
    var electrodomestic2 = Electrodomestic(100, "B", 15, "platejat")
    var electrodomestic3 = Electrodomestic(79, "D", 30, "color")
    var electrodomestic4 = Electrodomestic(35, "A", 2, "color")
    var electrodomestic5 = Electrodomestic(35, "A", 2, "platejat")
    var electrodomestic6 = Electrodomestic(35, "A", 2, "blanc  ")
    var rentadora7 = Rentadora(35, "A", 2, "color", 5)
    var rentadora8 = Rentadora(35, "A", 2, "blanc  ", 8)
    var televisio9 = Televisio(35, "A", 2, "platejat", 52)
    var televisio10 = Televisio(35, "A", 2, "blanc  ", 28)
    val electrodomestics = arrayOf(
        electrodomestic1,
        electrodomestic2,
        electrodomestic3,
        electrodomestic4,
        electrodomestic5,
        electrodomestic6,
        rentadora7,
        rentadora8,
        televisio9,
        televisio10
    )
    var totalElectrodomesticBase = 0
    var totalElectrodomestic = 0
    var totalRentadoresBase = 0
    var totalRentadores = 0
    var totalTelevisioBase = 0
    var totalTelevisio = 0
    var count = 1
    for (i in electrodomestics) {
        println(
            "Electrodomèstic ${count}:\n" +
                    "Preu base: ${i.preuBase}€\n" +
                    "Color: ${i.color}\n" +
                    "Consum: ${i.consum}\n" +
                    "Pes: ${i.pes}kg\n" +
                    "Preu final: ${i.preuFinal}\n"
        )
        if (i is Rentadora) {
            totalRentadoresBase+= i.preuBase
            totalRentadores += i.preuFinal
        } else if (i is Televisio) {
            totalTelevisioBase += i.preuBase
            totalTelevisio += i.preuFinal
        } else {
            totalElectrodomestic += i.preuFinal
            totalElectrodomesticBase += i.preuBase
        }
        count++
    }
    print("Electrodomèstics:\n" +
            "Preu base: ${totalElectrodomesticBase}€\n" +
            "Preu final: ${totalElectrodomestic}€\n" +
            "Rentadores:\n" +
            "Preu base: ${totalRentadoresBase}€\n" +
            "Preu final: ${totalRentadores}€\n" +
            "Televisions:\n" +
            "Preu base: ${totalTelevisioBase}€\n" +
            "Preu final: ${totalTelevisio}€\n")
}